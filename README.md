# base

A base for starting new javascript projects on top of! Clone,
customise & start building!

## Inspiration

It's pretty cumbersome to setup a few utilities like commit linting,
automatic changelog generation, code linting, formatting etc. This project
aims to partially solve these problems by having a basic template project
that can be forked & modified for any javascript-based project's needs.

## Setup

- Clone the repository
- Run `npm install` to install dependencies
- Start customizing to suit your project's needs!
